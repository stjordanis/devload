# devload

Commandline device driver loading program, supports UMBs


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DEVLOAD.LSM

<table>
<tr><td>title</td><td>devload</td></tr>
<tr><td>version</td><td>3.25a</td></tr>
<tr><td>entered&nbsp;date</td><td>2011-08-05</td></tr>
<tr><td>description</td><td>Commandline device driver loading program, supports UMBs</td></tr>
<tr><td>keywords</td><td>commandline, device, driver, load, ctload, devload</td></tr>
<tr><td>author</td><td>David Woodhouse &lt;Dave _AT_ infradead.org&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;e.auer _AT_ jpberlin.de&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.auersoft.eu/soft/</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.infradead.org/devload</td></tr>
<tr><td>platforms</td><td>DOS, NASM, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
